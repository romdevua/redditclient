package com.ringlabs.reddit.domain.entity;

public class StoryEntity {

    // key used to dispatch comments amount updates
    public static final String COMMENTS = "comments";

    public enum SourceType {
        IMAGE, MP4, NONE
    }

    private String id;
    private String name;
    private String author;
    private String title;
    private String thumbnail;
    private String source;
    private SourceType sourceType;
    private Long createdAt;
    private Integer commentsCount;

    public StoryEntity(String id, String name, String author,
                       String title, String thumbnail,
                       String source, SourceType sourceType,
                       Long createdAt, Integer commentsCount) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.title = title;
        this.thumbnail = thumbnail;
        this.source = source;
        this.sourceType = sourceType;
        this.createdAt = createdAt;
        this.commentsCount = commentsCount;
    }

    public String getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getSource() {
        return source;
    }

    public SourceType getSourceType() {
        return sourceType;
    }

    public long getCreatedAtMillis() {
        return createdAt * 1000;
    }
    public Integer getCommentsCount() {
        return commentsCount;
    }

    public String getName() {
        return name;
    }
}
