package com.ringlabs.reddit.domain.repository;

import com.ringlabs.reddit.domain.entity.StoryEntity;

import java.util.List;

import io.reactivex.Observable;

public interface StoriesRepository {
    Observable<List<StoryEntity>> getStories(String after);
}
