package com.ringlabs.reddit.domain.interactor.user;


import io.reactivex.Observable;
import io.reactivex.Scheduler;

import com.ringlabs.reddit.domain.entity.StoryEntity;
import com.ringlabs.reddit.domain.interactor.UseCase;
import com.ringlabs.reddit.domain.repository.StoriesRepository;

import java.util.List;

public class GetStoriesUseCase extends UseCase<List<StoryEntity>> {

    private StoriesRepository storiesRepository;

    private String after;

    public GetStoriesUseCase(Scheduler executionScheduler,
                             Scheduler postExecutionScheduler,
                             StoriesRepository storiesRepository) {
        super(executionScheduler, postExecutionScheduler);
        this.storiesRepository = storiesRepository;
    }

    public void setParams(String after) {
        this.after = after;
    }

    @Override
    protected Observable<List<StoryEntity>> buildUseCaseObservable() {
        return this.storiesRepository.getStories(this.after);
    }
}
