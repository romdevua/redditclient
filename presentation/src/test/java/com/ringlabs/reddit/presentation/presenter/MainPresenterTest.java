package com.ringlabs.reddit.presentation.presenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ringlabs.reddit.domain.entity.StoryEntity;
import com.ringlabs.reddit.domain.interactor.user.GetStoriesUseCase;
import com.ringlabs.reddit.presentation.Contracts;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Created on 28.04.17.
 */
public class MainPresenterTest {

    @Mock GetStoriesUseCase getStoriesUseCase;
    @Mock Contracts.Main.ViewMVP mainView;

    MainPresenter mainPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mainPresenter = new MainPresenter(getStoriesUseCase);
        mainPresenter.attachView(mainView);
    }

    @Test
    public void applyParams() {
        mainPresenter.getStories("", true);
        verify(this.getStoriesUseCase).setParams("");
    }

    @Test
    public void showStoryImage() {
        StoryEntity storyEntity = new StoryEntity(
                "testId", "testName", "testAuthor",
                "testTitle", "testThumb", "testSource",
                StoryEntity.SourceType.IMAGE, 1990L, 15);

        mainPresenter.handleSelection(storyEntity);
        verify(this.mainView).showStoryImg(
                "testThumb", StoryEntity.SourceType.IMAGE,
                "testSource");
    }

    @Test
    public void doNotShowStoryWithoutThumb() {
        StoryEntity storyEntity = new StoryEntity(
                "testId", "testName", "testAuthor",
                "testTitle", "", "testSource",
                StoryEntity.SourceType.IMAGE, 1990L, 15);

        mainPresenter.handleSelection(storyEntity);
        verifyNoMoreInteractions(this.mainView);
    }

    @Test
    public void doNotShowStoryWithoutThumbSource() {
        StoryEntity storyEntity = new StoryEntity(
                "testId", "testName", "testAuthor",
                "testTitle", "testThumb", "",
                StoryEntity.SourceType.IMAGE, 1990L, 15);

        mainPresenter.handleSelection(storyEntity);
        verifyNoMoreInteractions(this.mainView);
    }
}
