package com.ringlabs.reddit.presentation;

import com.ringlabs.reddit.presentation.dependency.component.DependencyInjector;

public class TestMockerApplication extends BaseApplication {

    @Override
    public DependencyInjector getDependencyInjector() {
        return DaggerTestMockerComponent.builder()
                .applicationComponent(this.applicationComponent)
                .testMockerModule(new TestMockerModule()).build();
    }
}
