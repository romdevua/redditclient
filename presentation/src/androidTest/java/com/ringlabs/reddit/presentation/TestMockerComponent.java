package com.ringlabs.reddit.presentation;

import com.ringlabs.reddit.presentation.dependency.ActivityScope;
import com.ringlabs.reddit.presentation.dependency.component.ApplicationComponent;
import com.ringlabs.reddit.presentation.dependency.component.DependencyInjector;

import dagger.Component;

@ActivityScope
@Component(modules = TestMockerModule.class, dependencies = ApplicationComponent.class)
public interface TestMockerComponent extends DependencyInjector {}
