package com.ringlabs.reddit.presentation;

import android.app.Application;

import com.ringlabs.reddit.presentation.dependency.component.ApplicationComponent;
import com.ringlabs.reddit.presentation.dependency.component.DaggerApplicationComponent;
import com.ringlabs.reddit.presentation.dependency.component.DependencyInjector;
import com.ringlabs.reddit.presentation.dependency.module.ApplicationModule;

public class BaseApplication extends Application {

    protected ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        this.initializeInjector();
    }

    protected void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                                        .applicationModule(new ApplicationModule(this))
                                        .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }

    public DependencyInjector getDependencyInjector() {
        return this.applicationComponent;
    }
}
