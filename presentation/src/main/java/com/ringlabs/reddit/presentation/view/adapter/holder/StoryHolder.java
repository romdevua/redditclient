package com.ringlabs.reddit.presentation.view.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ringlabs.reddit.domain.entity.StoryEntity;
import com.ringlabs.reddit.presentation.R;
import com.ringlabs.reddit.presentation.view.adapter.StoriesAdapter;
import com.ringlabs.reddit.presentation.view.adapter.util.ItemSelectionListener;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created on 08.05.17.
 */

public class StoryHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.image)
    ImageView image;
    @Bind(R.id.title)
    TextView title;
    @Bind(R.id.author)
    TextView author;
    @Bind(R.id.commentCount)
    TextView commentsCount;
    @Bind(R.id.createdAt)
    TextView createdAt;
    private ItemSelectionListener selectionListener;

    public StoryHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(StoryEntity story) {
        title.setText(story.getTitle());
        author.setText(story.getAuthor());
        image.postDelayed(()->Glide.with(itemView.getContext())
                .load(story.getThumbnail())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.reddit_holder)
                .centerCrop().into(image), 20);
        updateComments(story.getCommentsCount());
        updateTime(story);
    }

    @OnClick(R.id.content)
    void handleContentClick() {
        // dispatch story selection
    }

    @OnClick(R.id.image)
    void handleImageSelection() {
        if (selectionListener != null) selectionListener
                .onItemSelected(getAdapterPosition());
    }

    public void updateTime(StoryEntity story) {
        String niceDateStr = DateUtils.getRelativeTimeSpanString(
                story.getCreatedAtMillis(),
                Calendar.getInstance().getTimeInMillis(),
                DateUtils.MINUTE_IN_MILLIS)
                .toString();
        createdAt.setText(niceDateStr);
    }

    public void updateComments(int commentsAmount) {
        commentsCount.setText(String.valueOf(commentsAmount));
    }

    public void setSelectionListener(ItemSelectionListener listener) {
        this.selectionListener = listener;
    }
}
