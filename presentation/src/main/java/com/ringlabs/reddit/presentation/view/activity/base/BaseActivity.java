package com.ringlabs.reddit.presentation.view.activity.base;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.viewstate.MvpViewStateActivity;
import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;
import com.ringlabs.reddit.data.net.NetworkUtil;
import com.ringlabs.reddit.presentation.BaseApplication;
import com.ringlabs.reddit.presentation.Contracts.BaseMvpView;
import com.ringlabs.reddit.presentation.R;
import com.ringlabs.reddit.presentation.dependency.component.ApplicationComponent;
import com.ringlabs.reddit.presentation.view.activity.MainActivity;

import butterknife.ButterKnife;

public abstract class BaseActivity<V extends BaseMvpView, P extends MvpPresenter<V>,
        VS extends ViewState<V>> extends MvpViewStateActivity<V, P, VS> {

    private ProgressDialog progressDialog;
    private Toast toastMessage;

    protected abstract int getContentView();

    public ApplicationComponent getAppComponent() {
        return ((BaseApplication)getApplication()).getApplicationComponent();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        callInjector();
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        ButterKnife.bind(this);
    }

    protected abstract void callInjector();

    public boolean isLoaderShowing() {
        if (this.progressDialog == null) return false;
        return this.progressDialog.isShowing();
    }

    public void showLoader() {
        if (this.progressDialog == null) this.progressDialog = new ProgressDialog(this);
        this.progressDialog.show();
    }

    public void hideLoader() {
        if (this.progressDialog != null) this.progressDialog.dismiss();
        this.progressDialog = null;
    }

    public void showConnectivityDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setIcon(R.drawable.reddit_small_icon);
        dialogBuilder.setTitle(R.string.connection_dialog_title);
        dialogBuilder.setMessage(R.string.connection_dialog_msg);
        dialogBuilder.setPositiveButton(R.string.enable, (dialog1, which) -> {
            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
        });
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
        registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if (NetworkUtil.isConnected(BaseActivity.this)) {
                            dialog.dismiss();
                        }
                    }
                },
                new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    protected void hideKeyboard() {
        hideKeyboard(getCurrentFocus());
    }

    protected void hideKeyboard(View view) {
        if (view == null) {
            view = new View(this);
        }
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void toast(String msg) {
        toast(msg, Toast.LENGTH_SHORT);
    }

    public void toast(Integer resId) {
        toast(getString(resId), Toast.LENGTH_SHORT);
    }

    public void toast(String msg, int duration) {
        if (toastMessage != null) {
            toastMessage.cancel();
        }
        toastMessage = Toast.makeText(this, msg, duration);
        toastMessage.show();
    }

    public void closeAndDisplayLogin() {
        //show login screen
    }

}
