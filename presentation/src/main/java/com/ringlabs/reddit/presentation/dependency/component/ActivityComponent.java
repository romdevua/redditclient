package com.ringlabs.reddit.presentation.dependency.component;

import com.ringlabs.reddit.presentation.dependency.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(dependencies = ApplicationComponent.class)
public interface ActivityComponent {

}
