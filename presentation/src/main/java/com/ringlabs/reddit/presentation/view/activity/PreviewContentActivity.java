package com.ringlabs.reddit.presentation.view.activity;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.devbrackets.android.exomedia.ui.widget.VideoView;
import com.ringlabs.reddit.presentation.R;
import com.ringlabs.reddit.domain.entity.StoryEntity.SourceType;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created on 09.05.17.
 */

public class PreviewContentActivity extends AppCompatActivity {

    private static final String THUMB_URL = "thumb";
    private static final String SOURCE_TYPE = "type";
    private static final String SOURCE_URL = "source";
    private static final int EXT_STORAGE_REQ = 389;

    @Bind(R.id.previewImg)
    protected ImageView image;
    @Bind(R.id.videoView)
    protected VideoView videoView;
    @Bind(R.id.progressBar)
    protected View progressBar;

    public static Intent getStartIntent(Context context, String thumbUrl,
                                        SourceType sourceType,
                                        String sourceUrl) {
        Intent intent = new Intent(context, PreviewContentActivity.class);
        intent.putExtra(THUMB_URL, thumbUrl);
        intent.putExtra(SOURCE_TYPE, sourceType);
        intent.putExtra(SOURCE_URL, sourceUrl);
        return intent;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview_activity);
        ButterKnife.bind(this);
        showSource();
    }

    private void showSource() {
        switch (getSourceType()) {
            case IMAGE:
                Glide.with(this)
                        .load(getSourceUrl())
                        .thumbnail(Glide.with(this).load(getThumbUrl()))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .fitCenter().into(image);
                break;
            case MP4:
                Glide.with(this)
                        .load(getThumbUrl())
                        .fitCenter().into(image);
                setupVideoView();
                break;
        }
    }

    private void setupVideoView() {
        progressBar.setVisibility(VISIBLE);
        videoView.setOnPreparedListener(() -> {
            progressBar.setVisibility(GONE);
            videoView.start();
            videoView.setVisibility(VISIBLE);
            image.setVisibility(GONE);
        });
        videoView.setOnCompletionListener(
                () -> videoView.restart());
        videoView.setVideoURI(Uri.parse(getSourceUrl()));
    }

    private String getThumbUrl() {
        return getIntent().getStringExtra(THUMB_URL);
    }

    private SourceType getSourceType() {
        return (SourceType) getIntent().getSerializableExtra(SOURCE_TYPE);
    }

    private String getSourceUrl() {
        return getIntent().getStringExtra(SOURCE_URL);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.preview_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_download) {
            startDownloadWithPermission();
        }
        return true;
    }

    private void startDownloadWithPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXT_STORAGE_REQ);
        } else {
            startSourceDownload();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == EXT_STORAGE_REQ
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startSourceDownload();
        }
    }

    private void startSourceDownload() {
        Toast.makeText(getBaseContext(),
                getString(R.string.download_started),
                Toast.LENGTH_SHORT).show();
        Uri downloadUri = Uri.parse(getSourceUrl());
        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(downloadUri);
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request
                .VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
                getContentTitle());
        downloadManager.enqueue(request);
        finish();
    }

    private String getContentTitle() {
        String extension = "";
        switch (getSourceType()) {
            case MP4: extension = "mp4"; break;
            case IMAGE: extension = "jpg"; break;
        }
        return String.format("reddit_%s.%s",
                String.valueOf(System.currentTimeMillis()),
                extension);
    }
}
