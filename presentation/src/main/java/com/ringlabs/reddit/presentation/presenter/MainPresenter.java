package com.ringlabs.reddit.presentation.presenter;

import javax.inject.Inject;

import com.ringlabs.reddit.data.net.NoConnectionError;
import com.ringlabs.reddit.domain.entity.StoryEntity;
import com.ringlabs.reddit.domain.interactor.user.GetStoriesUseCase;
import com.ringlabs.reddit.presentation.Contracts.Main;
import com.ringlabs.reddit.presentation.presenter.base.BasePresenter;

import java.util.List;

import io.reactivex.observers.DisposableObserver;

/**
 * Created on 27.04.17.
 */

public class MainPresenter extends BasePresenter<Main.ViewMVP>
        implements Main.PresenterMVP {

    private GetStoriesUseCase getStoriesUseCase;
    private StoriesSubscriber getStoriesObserver;
    private boolean isInPreview = false;
    private String lastRequestId = "";

    @Inject
    MainPresenter(GetStoriesUseCase getStoriesUseCase) {
        this.getStoriesUseCase = getStoriesUseCase;
    }

    @Override
    public void getStories(String after, boolean force) {
        if (!lastRequestId.equals(after) || force) {
            lastRequestId = after;
            getStoriesUseCase.setParams(after);
            getStoriesObserver = new StoriesSubscriber(force);
            getStoriesUseCase.execute(getStoriesObserver);
        }
    }

    @Override
    public void handleSelection(StoryEntity story) {
        if (isPreviewAvailable(story)) {
            isInPreview = true;
            view.showStoryImg(
                    story.getThumbnail(),
                    story.getSourceType(),
                    story.getSource());
        }
    }

    @Override
    public void previewFinished() {
        isInPreview = false;
    }

    private boolean isPreviewAvailable(StoryEntity story) {
        String sourceImg = story.getSource();
        String thumbImg = story.getThumbnail();
        return !isInPreview
                && !(sourceImg == null || sourceImg.isEmpty())
                && !(thumbImg == null || thumbImg.isEmpty());
    }

    private class StoriesSubscriber extends DisposableObserver<List<StoryEntity>> {

        private boolean force;

        StoriesSubscriber(boolean force) {
            this.force = force;
        }

        @Override
        protected void onStart() {
            super.onStart();
            if (force) MainPresenter.this.view.showLoader();
        }

        @Override
        public void onNext(List<StoryEntity> stories) {
            MainPresenter.this.view.hideEmptyView();
            MainPresenter.this.view.showStories(stories, force);
        }

        @Override
        public void onError(Throwable e) {
            MainPresenter.this.view.hideLoader();
            lastRequestId = "";

            if (e instanceof NoConnectionError) {
                MainPresenter.this.view.showConnectionIssue();
            } else {
                MainPresenter.this.view.showMessage("Error " + e.getMessage());
            }
        }

        @Override
        public void onComplete() {
            MainPresenter.this.view.hideLoader();
            lastRequestId = "";
        }

    }
}
