package com.ringlabs.reddit.presentation.dependency.component;

import com.ringlabs.reddit.domain.interactor.user.GetStoriesUseCase;
import com.ringlabs.reddit.presentation.view.activity.MainActivity;

public interface DependencyInjector {

    void inject(GetStoriesUseCase getStoriesUseCase);

    void inject(MainActivity mainActivity);

}
