package com.ringlabs.reddit.presentation.view.adapter;

import android.os.Bundle;
import android.support.v4.app.BundleCompat;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.ringlabs.reddit.domain.entity.StoryEntity;
import com.ringlabs.reddit.presentation.R;
import com.ringlabs.reddit.presentation.view.adapter.holder.StoryHolder;
import com.ringlabs.reddit.presentation.view.adapter.util.ItemSelectionListener;
import com.ringlabs.reddit.presentation.view.adapter.util.StoriesDiffUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 08.05.17.
 */

public class StoriesAdapter extends RecyclerView.Adapter<StoryHolder> {

    private List<StoryEntity> stories;

    private ItemSelectionListener selectionListener;

    public StoriesAdapter(ItemSelectionListener listener) {
        this.selectionListener = listener;
        this.stories = new ArrayList<>();
    }

    @Override
    public StoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.story_item, parent, false);
        StoryHolder holder = new StoryHolder(view);
        holder.setSelectionListener(selectionListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(StoryHolder holder, int position) {
        holder.bind(stories.get(position));
    }

    @Override
    public void onBindViewHolder(StoryHolder holder, int position, List<Object> payloads) {
        if (!payloads.isEmpty()) {
            Bundle bundle = (Bundle) payloads.get(0);
            holder.updateComments(bundle.getInt(StoryEntity.COMMENTS));
        } else {
            onBindViewHolder(holder, position);
        }
        holder.updateTime(stories.get(position));
    }

    @Override
    public int getItemCount() {
        return stories == null ? 0 : stories.size();
    }

    public void addItems(List<StoryEntity> stories) {
        int sizeBeforeAdd = this.stories.size();
        this.stories.addAll(stories);
        notifyItemRangeInserted(sizeBeforeAdd, stories.size());
    }

    public void   swapItems(List<StoryEntity> stories) {
        DiffUtil.DiffResult diffResult = DiffUtil
                .calculateDiff(new StoriesDiffUtil(this.stories, stories));
        this.stories.clear();
        this.stories.addAll(stories);
        diffResult.dispatchUpdatesTo(this);
    }

    public StoryEntity getItem(int position) {
        return stories.get(position);
    }

    public List<StoryEntity> getItems() {
        return stories;
    }
}
