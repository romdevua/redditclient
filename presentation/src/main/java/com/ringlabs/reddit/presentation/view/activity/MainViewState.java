package com.ringlabs.reddit.presentation.view.activity;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;
import com.ringlabs.reddit.domain.entity.StoryEntity;
import com.ringlabs.reddit.presentation.Contracts;

import java.util.List;

/**
 * Created on 08.05.17.
 */

class MainViewState implements ViewState<Contracts.Main.ViewMVP> {

    private List<StoryEntity> stories;
    private int firstVisibleItemPosition;

    public MainViewState(List<StoryEntity> stories, int firstVisibleItemPosition) {
        this.firstVisibleItemPosition = firstVisibleItemPosition;
        this.stories = stories;
    }

    @Override
    public void apply(Contracts.Main.ViewMVP view, boolean retained) {
        if (retained) {
            view.showStories(stories, firstVisibleItemPosition);
            if (stories != null && !stories.isEmpty()) {
                view.hideEmptyView();
            }
        }
    }

    void updateFirstVisiblePos(int firstVisibleItemPosition) {
        this.firstVisibleItemPosition = firstVisibleItemPosition;
    }

    void updateStories(List<StoryEntity> stories) {
        this.stories = stories;
    }
}
