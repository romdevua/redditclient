package com.ringlabs.reddit.presentation.view.adapter.util;

import android.os.Bundle;
import android.support.v7.util.DiffUtil;

import com.ringlabs.reddit.domain.entity.StoryEntity;

import java.util.List;

/**
 * Created on 08.05.17.
 */

public class StoriesDiffUtil extends DiffUtil.Callback {

    private List<StoryEntity> oldStories;
    private List<StoryEntity> newStories;

    public StoriesDiffUtil(List<StoryEntity> oldStories, List<StoryEntity> newStories) {
        this.oldStories = oldStories;
        this.newStories = newStories;
    }

    @Override
    public int getOldListSize() {
        return oldStories == null ? 0 : oldStories.size();
    }

    @Override
    public int getNewListSize() {
        return newStories == null ? 0 : newStories.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldStories.get(oldItemPosition).getId()
                .equals(newStories.get(newItemPosition).getId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldStories.get(oldItemPosition).getCommentsCount().intValue()
                == newStories.get(newItemPosition).getCommentsCount().intValue();
    }

    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        StoryEntity newStory = newStories.get(newItemPosition);
        StoryEntity oldStory = oldStories.get(oldItemPosition);
        Bundle diffBundle = new Bundle();
        if (newStory.getCommentsCount().intValue() != oldStory.getCommentsCount().intValue()) {
            diffBundle.putInt(StoryEntity.COMMENTS, newStory.getCommentsCount());
        }
        if (diffBundle.size() == 0) {
            return null;
        } else {
            return diffBundle;
        }
    }
}
