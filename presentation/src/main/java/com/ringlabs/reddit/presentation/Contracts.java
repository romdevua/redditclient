package com.ringlabs.reddit.presentation;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.ringlabs.reddit.domain.entity.StoryEntity;
import com.ringlabs.reddit.domain.entity.StoryEntity.SourceType;

import java.util.List;

/**
 * Created on 27.04.17.
 */

public interface Contracts {

    interface BaseMvpView extends MvpView {
        void showLoader();

        void hideLoader();

        void showMessage(String message);

        void showConnectionIssue();
    }

    interface Main {
        interface ViewMVP extends BaseMvpView {
            void showStories(List<StoryEntity> storyEntities, boolean force);

            void showStories(List<StoryEntity> storyEntities, int firstItemPosition);

            void showStoryImg(String source, SourceType sourceType, String thumb);

            void hideEmptyView();

        }

        interface PresenterMVP extends MvpPresenter<ViewMVP> {
            void getStories(String after, boolean force);

            void handleSelection(StoryEntity story);

            void previewFinished();
        }
    }
}
