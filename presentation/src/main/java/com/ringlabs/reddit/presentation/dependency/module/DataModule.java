package com.ringlabs.reddit.presentation.dependency.module;

import android.content.Context;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.ringlabs.reddit.data.net.ApiService;
import com.ringlabs.reddit.data.net.ConnectivityInterceptor;
import com.ringlabs.reddit.data.net.RestApi;
import com.ringlabs.reddit.data.repository.StoriesDataRepository;
import com.ringlabs.reddit.domain.repository.StoriesRepository;
import com.ringlabs.reddit.presentation.BuildConfig;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class DataModule {

    @Provides
    @Singleton
    RestApi provideRestApi(Context context) {

        ConnectivityInterceptor connectivity = new ConnectivityInterceptor(context);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                : HttpLoggingInterceptor.Level.NONE);

        OkHttpClient client = new OkHttpClient().newBuilder()
                .addInterceptor(connectivity)
                .addInterceptor(logging)
                .build();

        GsonConverterFactory factory = GsonConverterFactory.create(new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .disableHtmlEscaping()
                .create());

        return new Retrofit.Builder()
                .baseUrl(RestApi.URL_BASE)
                .addConverterFactory(factory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
                .create(RestApi.class);
    }

    @Provides
    @Singleton
    ApiService provideApiService(RestApi api) {
        return new ApiService(api);
    }

    @Provides
    @Singleton
    StoriesRepository provideUserRepository(ApiService apiService) {
        return new StoriesDataRepository(apiService);
    }

}
