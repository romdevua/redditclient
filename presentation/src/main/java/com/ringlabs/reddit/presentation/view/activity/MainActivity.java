package com.ringlabs.reddit.presentation.view.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ringlabs.reddit.domain.entity.StoryEntity;
import com.ringlabs.reddit.domain.entity.StoryEntity.SourceType;
import com.ringlabs.reddit.presentation.Contracts.Main;
import com.ringlabs.reddit.presentation.R;
import com.ringlabs.reddit.presentation.presenter.MainPresenter;
import com.ringlabs.reddit.presentation.view.activity.base.BaseActivity;
import com.ringlabs.reddit.presentation.view.adapter.StoriesAdapter;
import com.ringlabs.reddit.presentation.view.adapter.util.ItemSelectionListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;

public class MainActivity extends BaseActivity<Main.ViewMVP, Main.PresenterMVP, MainViewState>
        implements Main.ViewMVP, SwipeRefreshLayout.OnRefreshListener, ItemSelectionListener {

    private static final int PREVIEW_IMG_REQ = 1001;

    @Inject
    protected MainPresenter mainPresenter;

    @Bind(R.id.toolbar)
    protected Toolbar toolbar;
    @Bind(R.id.emptyView)
    protected View emptyView;
    @Bind(R.id.recycler)
    protected RecyclerView recycler;
    @Bind(R.id.refreshLayout)
    protected SwipeRefreshLayout refreshLayout;

    private LinearLayoutManager layoutManager;
    private StoriesAdapter adapter;
    private PaginationScrollListener paginationListener;

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected void callInjector() {
        getAppComponent().inject(this);
    }

    @NonNull
    @Override
    public Main.PresenterMVP createPresenter() {
        return mainPresenter;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        initRecycler();
    }

    private void initRecycler() {
        layoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(layoutManager);
        adapter = new StoriesAdapter(this);
        recycler.setAdapter(adapter);
        paginationListener = new PaginationScrollListener();
        recycler.addOnScrollListener(paginationListener);
        refreshLayout.setOnRefreshListener(this);
        setRestoringViewState(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_email) {
            askSomeFeedback();
            return true;
        } else {
            return super.onOptionsItemSelected(item);

        }
    }

    private void askSomeFeedback() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.reddit_small_icon)
                .setTitle(R.string.feedback_alert_title)
                .setMessage(R.string.feedback_alert_msg)
                .setPositiveButton(R.string.yes, (dialog, which) -> sendFeedback())
                .setNegativeButton(R.string.no, null)
                .create().show();

    }

    public void sendFeedback() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto: poshta.rom@gmail.com"));
        startActivity(Intent.createChooser(emailIntent, "Send feedback"));
    }

    @Override
    public void showLoader() {
        refreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoader() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void showMessage(String message) {
        toast(message);
    }

    @Override
    public void showConnectionIssue() {
        showConnectivityDialog();
    }

    @Override
    public void showStories(List<StoryEntity> stories, boolean force) {
        if (force) {
            paginationListener.resetLastPosition();
            adapter.swapItems(stories);
        } else {
            adapter.addItems(stories);
        }
        viewState.updateStories(adapter.getItems());
    }

    @Override
    public void onRefresh() {
        presenter.getStories("", true);
    }

    @Override
    public void showStories(List<StoryEntity> stories, int firstItemPosition) {
        showStories(stories, true);
        paginationListener = new PaginationScrollListener();
        recycler.addOnScrollListener(paginationListener);
        recycler.post(()->layoutManager.scrollToPosition(firstItemPosition));
    }

    @Override
    public void showStoryImg(String thumb, SourceType sourceType, String source) {
        startActivityForResult(PreviewContentActivity
                .getStartIntent(this, thumb, sourceType, source), PREVIEW_IMG_REQ);
    }

    @Override
    public void hideEmptyView() {
        emptyView.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PREVIEW_IMG_REQ) presenter.previewFinished();
    }

    @NonNull
    @Override
    public MainViewState createViewState() {
        int firstVisiblePos = layoutManager.findFirstVisibleItemPosition();
        return new MainViewState(adapter.getItems(), firstVisiblePos);
    }

    @Override
    public void onNewViewStateInstance() {
        presenter.getStories("", true);
    }

    @Override
    public void onItemSelected(int position) {
        StoryEntity selectedStory = adapter.getItem(position);
        presenter.handleSelection(selectedStory);
    }

    private class PaginationScrollListener extends RecyclerView.OnScrollListener {

        private final static int THRESHOLD = 3;
       private static final int MAX_ITEMS = 50;

       int lastVisibleItemPosition = -1;

        void resetLastPosition() {
            lastVisibleItemPosition = -1;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int firstVisiblePos = layoutManager.findFirstVisibleItemPosition();
                viewState.updateFirstVisiblePos(firstVisiblePos);
            int newLastPosition = layoutManager.findLastVisibleItemPosition();
            if (newLastPosition >= lastVisibleItemPosition) {
                lastVisibleItemPosition = newLastPosition;
                int itemsInAdapter = adapter.getItemCount();
                int itemsLeft = itemsInAdapter - lastVisibleItemPosition;
                if (itemsLeft <= THRESHOLD && itemsInAdapter < MAX_ITEMS) {
                    presenter.getStories(adapter.getItem(itemsInAdapter-1).getName(), false);
                }
            }
        }

    }
}
