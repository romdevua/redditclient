package com.ringlabs.reddit.presentation.dependency.component;

import android.content.Context;
import android.content.SharedPreferences;

import io.reactivex.Scheduler;
import com.ringlabs.reddit.data.net.RestApi;
import com.ringlabs.reddit.domain.interactor.user.GetStoriesUseCase;
import com.ringlabs.reddit.domain.repository.StoriesRepository;
import com.ringlabs.reddit.presentation.dependency.module.ApplicationModule;
import com.ringlabs.reddit.presentation.dependency.module.DataModule;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = { ApplicationModule.class, DataModule.class })
public interface ApplicationComponent extends DependencyInjector {

    Context context();
    SharedPreferences sharedPreferences();

    @Named("UI")
    Scheduler uiScheduler();
    @Named("IO")
    Scheduler ioScheduler();

    RestApi restApi();
    StoriesRepository userRepository();
    GetStoriesUseCase getStoriesUseCase();
}
