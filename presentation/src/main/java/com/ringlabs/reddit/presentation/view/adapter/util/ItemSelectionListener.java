package com.ringlabs.reddit.presentation.view.adapter.util;

/**
 * Created on 09.05.17.
 */

public interface ItemSelectionListener {
    void onItemSelected(int position);
}