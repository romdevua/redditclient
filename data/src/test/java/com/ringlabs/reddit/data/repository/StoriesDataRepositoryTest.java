package com.ringlabs.reddit.data.repository;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.ringlabs.reddit.data.net.ApiService;
import com.ringlabs.reddit.data.net.RestApi;
import com.ringlabs.reddit.data.utils.TestUtils;
import com.ringlabs.reddit.domain.entity.StoryEntity;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import io.reactivex.observers.TestObserver;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static junit.framework.Assert.assertEquals;

@SuppressWarnings("unchecked")
public class StoriesDataRepositoryTest {

    private StoriesDataRepository storiesDataRepository;
    private TestObserver testObserver;
    private MockWebServer mockWebServer;
    private Gson gson;

    @Before
    public void setUp() throws IOException {
        this.gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        this.mockWebServer = new MockWebServer();
        this.mockWebServer.start();

        RestApi restApi = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("/"))
                .addConverterFactory(GsonConverterFactory.create(this.gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(RestApi.class);

        this.storiesDataRepository = new StoriesDataRepository(new ApiService(restApi));

        this.testObserver = new TestObserver();
    }

    @After
    public void tearDown() throws Exception {
        this.mockWebServer.shutdown();
    }

    @Test
    public void testGetStories() throws Exception {

        this.mockWebServer.enqueue(new MockResponse().setResponseCode(201).setBody(
                FileUtils.readFileToString(
                        TestUtils.getFileFromPath(this, "res/top.json"))));

        this.storiesDataRepository.getStories("").subscribe(this.testObserver);
        this.testObserver.awaitTerminalEvent();

        RecordedRequest request = this.mockWebServer.takeRequest();
        assertEquals("/top.json?after=&limit=10&raw_json=1", request.getPath());
        assertEquals("GET", request.getMethod());
    }

    @Test
    public void testGetStoriesAfter() throws Exception {

        this.mockWebServer.enqueue(new MockResponse().setResponseCode(201).setBody(
                FileUtils.readFileToString(
                        TestUtils.getFileFromPath(this, "res/top.json"))));

        this.storiesDataRepository.getStories("t3_ghs2gs").subscribe(this.testObserver);
        this.testObserver.awaitTerminalEvent();

        RecordedRequest request = this.mockWebServer.takeRequest();
        assertEquals("/top.json?after=t3_ghs2gs&limit=10&raw_json=1", request.getPath());
        assertEquals("GET", request.getMethod());
    }

    @Test
    public void testGetStoriesBatchSize() throws Exception {

        this.mockWebServer.enqueue(new MockResponse().setResponseCode(201).setBody(
                FileUtils.readFileToString(
                        TestUtils.getFileFromPath(this, "res/top.json"))));

        this.storiesDataRepository.getStories("").subscribe(this.testObserver);
        this.testObserver.awaitTerminalEvent();

        List<StoryEntity> stories = (List<StoryEntity>) ((List<Object>) testObserver
                .getEvents().get(0)).get(0);
        assertEquals(10, stories.size());
    }

}
