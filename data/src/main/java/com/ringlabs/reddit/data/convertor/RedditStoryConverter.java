package com.ringlabs.reddit.data.convertor;

import com.ringlabs.reddit.data.net.entity.RedditMediaVariants;
import com.ringlabs.reddit.data.net.entity.RedditPreview;
import com.ringlabs.reddit.data.net.entity.RedditStory;
import com.ringlabs.reddit.data.net.entity.RedditStoryData;
import com.ringlabs.reddit.domain.entity.StoryEntity;
import com.ringlabs.reddit.domain.entity.StoryEntity.SourceType;

/**
 * Created on 08.05.17.
 */

public class RedditStoryConverter {

    public StoryEntity convert(RedditStory redditStory) {
        RedditStoryData data = redditStory.getData();
        SourceType sourceType = getSourceType(data);
        return new StoryEntity(
                data.getId(),
                data.getName(),
                data.getAuthor(),
                data.getTitle(),
                getThumnailUrl(data),
                getSourceUrl(data, sourceType),
                sourceType,
                data.getCreatedUtc(),
                data.getNumComments());
    }

    private SourceType getSourceType(RedditStoryData data) {
        RedditPreview preview = data.getPreview();
        if (preview == null || preview.getImages().isEmpty()) {
            return SourceType.NONE;
        }
        RedditMediaVariants variants = preview.getImages()
                .get(0).getVariants();
        if (variants != null && variants.getMp4() != null) {
            return SourceType.MP4;
        }
        return SourceType.IMAGE;
    }

    private String getThumnailUrl(RedditStoryData data) {
        RedditPreview preview = data.getPreview();
        return (("self".equals(data.getThumbnail())
                || "default".equals(data.getThumbnail())) && preview != null)
                ? getSourceUrl(data, SourceType.IMAGE)
                : data.getThumbnail();
    }

    private String getSourceUrl(RedditStoryData data, SourceType sourceType) {
        switch (sourceType) {
            case IMAGE:
                return data.getPreview().getImages().get(0).getSource().getUrl();
            case MP4:
                return data.getPreview().getImages().get(0).getVariants()
                        .getMp4().getSource().getUrl();
            default:
                return "";
        }
    }
}
