package com.ringlabs.reddit.data.net;

import com.ringlabs.reddit.data.net.entity.RedditResponse;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created on 08.05.17.
 */

public class ApiService {

    private RestApi api;

    public ApiService(RestApi api) {
        this.api = api;
    }

    public Observable<RedditResponse> getTopStories(String after) {
        return api.getTopStories(after, 10, 1)
                .map(Response::body);
    }
}
