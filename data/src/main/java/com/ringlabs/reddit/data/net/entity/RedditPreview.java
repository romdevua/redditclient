package com.ringlabs.reddit.data.net.entity;

import java.util.List;

/**
 * Created on 08.05.17.
 */

public class RedditPreview {

    List<RedditComplexImage> images;

    public List<RedditComplexImage> getImages() {
        return images;
    }
}
