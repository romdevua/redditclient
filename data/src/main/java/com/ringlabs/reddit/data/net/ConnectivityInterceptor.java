package com.ringlabs.reddit.data.net;

import android.content.Context;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * Created on 09.05.17.
 */

public class ConnectivityInterceptor implements Interceptor {

    private Context context;

    public ConnectivityInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        if (NetworkUtil.isConnected(context)) {
            Response response = chain.proceed(chain.request());
            return response;
        } else {
            throw new NoConnectionError();
        }
    }
}
