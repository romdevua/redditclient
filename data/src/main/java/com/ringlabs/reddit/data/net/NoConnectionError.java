package com.ringlabs.reddit.data.net;

import java.io.IOException;

/**
 * Created on 09.05.17.
 */

public class NoConnectionError extends IOException {

    @Override
    public String getMessage() {
        return "Connection is not available";
    }
}
