package com.ringlabs.reddit.data.net.entity;

/**
 * Created on 08.05.17.
 */

public class RedditStory {

    private String kind;
    private RedditStoryData data;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public RedditStoryData getData() {
        return data;
    }

    public void setData(RedditStoryData data) {
        this.data = data;
    }
}