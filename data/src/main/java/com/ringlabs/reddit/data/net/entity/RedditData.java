package com.ringlabs.reddit.data.net.entity;

import java.util.List;

/**
 * Created on 08.05.17.
 */

public class RedditData {
    private String modhash;
    private List<RedditStory> children;
    private String after;
    private String before;

    public String getModhash() {
        return modhash;
    }

    public List<RedditStory> getChildren() {
        return children;
    }

    public String getAfter() {
        return after;
    }

    public String getBefore() {
        return before;
    }
}
