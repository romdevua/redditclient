package com.ringlabs.reddit.data.net.entity;

/**
 * Created on 08.05.17.
 */

public class RedditSingleImage {

    private String url;
    private Integer width;
    private Integer height;

    public String getUrl() {
        return url;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getHeight() {
        return height;
    }
}
