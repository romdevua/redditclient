package com.ringlabs.reddit.data.net;

import com.ringlabs.reddit.data.net.entity.RedditResponse;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface RestApi {

    String URL_BASE = "https://www.reddit.com";

    @GET("/top.json")
    Observable<Response<RedditResponse>> getTopStories(@Query("after") String afterId,
                                                       @Query("limit") Integer limit,
                                                       @Query("raw_json") Integer oneIfNeeded);

}
