package com.ringlabs.reddit.data.net.entity;

/**
 * Created on 08.05.17.
 */

public class RedditStoryData {

    private String id;
    private String name;
    private String author;
    private String title;
    private Long createdUtc;
    private Integer numComments;
    private String thumbnail;
    private RedditPreview preview;

    public String getId() {
        return id;
    }

    // used as value for parameter "after"
    // to retrieve next stories batch
    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public Long getCreatedUtc() {
        return createdUtc;
    }

    public Integer getNumComments() {
        return numComments;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public RedditPreview getPreview() {
        return preview;
    }
}
