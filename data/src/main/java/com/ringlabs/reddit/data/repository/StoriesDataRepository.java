package com.ringlabs.reddit.data.repository;

import com.ringlabs.reddit.data.convertor.RedditStoryConverter;
import com.ringlabs.reddit.data.net.ApiService;
import com.ringlabs.reddit.domain.entity.StoryEntity;
import com.ringlabs.reddit.domain.repository.StoriesRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class StoriesDataRepository implements StoriesRepository {

    private final ApiService apiService;
    private final RedditStoryConverter converter;

    @Inject
    public StoriesDataRepository(ApiService apiService) {
        this.apiService = apiService;
        this.converter =new RedditStoryConverter();
    }

    @Override
    public Observable<List<StoryEntity>> getStories(String after) {
        return this.apiService.getTopStories(after)
                .map(it -> it.getData().getChildren())
                .flatMapIterable(list -> list)
                .map(converter::convert)
                .toList().toObservable();
    }
}
