package com.ringlabs.reddit.data.net.entity;

/**
 * Created on 08.05.17.
 */

public class RedditResponse {

    private String kind;
    private RedditData data;

    public String getKind() {
        return kind;
    }

    public RedditData getData() {
        return data;
    }
}
