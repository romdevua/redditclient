package com.ringlabs.reddit.data.net.entity;

import java.util.List;

/**
 * Created on 08.05.17.
 */

public class RedditComplexImage {

    private RedditSingleImage source;
    private List<RedditSingleImage> resolutions;
    private RedditMediaVariants variants;

    public RedditSingleImage getSource() {
        return source;
    }

    public List<RedditSingleImage> getResolutions() {
        return resolutions;
    }

    public RedditMediaVariants getVariants() {
        return variants;
    }
}
